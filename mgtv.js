"use strict";
var _createClass = function() {
    function o(e, t) {
        for (var i = 0; i < t.length; i++) {
            var o = t[i];
            o.enumerable = o.enumerable || !1,
            o.configurable = !0,
            "value"in o && (o.writable = !0),
            Object.defineProperty(e, o.key, o)
        }
    }
    return function(e, t, i) {
        return t && o(e.prototype, t),
        i && o(e, i),
        e
    }
}();
function _classCallCheck(e, t) {
    if (!(e instanceof t))
        throw new TypeError("Cannot call a class as a function")
}
honey.def("lib:jquery", function(p) {
    var f = p.EquipmentPlugin
      , h = p.VideoPlayerTPL
      , v = p.PLOG
      , m = p.Utils.debounce
      , y = p.Utils.throttle
      , d = p.videoErrorUI
      , s = ["94l6w226e", "94l6w226d", "94l6w226c", "94l6w226b", "94l6w226a", "94l6w2269", "94l6w2268", "94l6w226n", "94l6w226m", "95exclax3", "95exclax2", "95exclax1", "95exclax0", "95exclawz", "95exclawy", "95exclawx", "95exclaww", "95exclaxb", "95exclaxa", "95exclapz", "95exclapy", "95exclapx", "95exclapw", "95exclapv", "95exclapu", "95exclapt", "95exclaps", "95exclaq7", "95exclaq6", "95exclaiv", "90f12f8na", "90f12f8n9", "90f12f8n8", "90f12f8n7", "90f12f8n6", "90f12f8n5", "90f12f8n4", "90f12f8nj", "90f12f8ni", "90f945a4n", "90f945a4m", "90f945a4l", "90f945a4k", "90f945a4j", "90f945a4i", "90f945a4h", "90f945a4g", "90f945a4v", "90f945a4u", "90f9459xj", "90f9459xi", "90f9459xh", "90f9459xg", "90f9459xf", "90f9459xe", "90f9459xd", "90f9459xc", "90f9459xr", "90f9459xq", "90f9459qf", "90f9459qe", "90f9459qd", "90f9459qc", "90f9459qb", "90f9459qa", "90f9459q9", "90f9459q8", "90f9459qn", "90f9459qm", "90f945b47", "90f12gp6u", "90f12gp6t", "90f12gp6s", "90f12gp6r", "90f12gp6q", "90f12gp6p", "90f12gp6o", "90f12gp73", "90f12gp72", "90f8tgeiv", "90f8tgeiu", "90f8tgeit", "90f8tgeis", "90f8tgeir", "90f8tgeiq", "90f8tgeip", "90f8tgeio", "90f8tgej3", "90f8tgej2", "90f8tgebr", "90f8tgebq", "90f8tgebp", "90f8tgebo", "90f8tgebn", "90f8tgebm", "90f8tgebl", "90f8tgebk", "90f8tgebz", "90f8tgeby", "90f8tge4n", "90f8tge4m", "90f8tge4l", "90f8tge4k", "90f8tge4j", "90f8tge4i", "90f8tge4h", "90f8tge4g", "90f8tge4v", "90f8tge4u", "90f8tgfif", "90f62oqfm", "90f945b46", "90f945b45", "90f945b44", "90f945b43", "90f945b42", "90f945b41", "90f945b40", "90f945b4f", "90f945b4e", "90f945ax3", "90f945ax2", "90f945ax1", "90f945ax0", "90f945awz", "90f945awy", "90f945awx", "90f945aww", "90f945axb", "90f945axa", "90f945apz"]
      , r = {
        box: "mgtv-player-wrap",
        video: "mgtv-video-income",
        autoplay: !1,
        theme: "#b7daff",
        loop: !1,
        lang: -1 !== navigator.language.indexOf("zh") ? "zh" : "en",
        screenshot: !1,
        hotkey: !0,
        preload: "auto",
        apiBackend: ""
    }
      , l = "5.5.9"
      , i = "5.5.9"
      , u = .5
      , c = {
        DEBUG_SWITCH: 1,
        ANTI_STEALING_LINK: 1,
        DANMAKU_SWITCH: 1,
        P2P_SWITCH: 1,
        SHARE_SWITCH: 0,
        AUTOPLAY: 1,
        USE_HOT_KEY: 1,
        USE_CONTEXT_MENU: 1,
        WEB_JUMP: 0,
        ALLOW_FULL_SCREEN: 1,
        ALLOW_FLOAT_WINDOW: 1
    }
      , o = (f.ua.ipad,
    function() {
        if (window.FLASH_MGTV_FUN && window.FLASH_MGTV_FUN.getPlayerNextId)
            return window.FLASH_MGTV_FUN.getPlayerNextId()
    }
    )
      , g = function() {
        p.MGTV_VIDEO_FUN && p.MGTV_VIDEO_FUN.getPlayEvent && p.MGTV_VIDEO_FUN.getPlayEvent()
    }
      , e = function() {
        p.MGTV_VIDEO_FUN && p.MGTV_VIDEO_FUN.getStopEvent && p.MGTV_VIDEO_FUN.getStopEvent()
    }
      , E = function(e, t) {
        p.MGTV_VIDEO_FUN && p.MGTV_VIDEO_FUN.getSeekEvent && p.MGTV_VIDEO_FUN.getSeekEvent(e, t)
    }
      , T = function(e) {
        window.FLASH_FLOATIONPLAYERWINDOW && window.FLASH_FLOATIONPLAYERWINDOW(e)
    }
      , b = function(e) {
        window.clipStart && window.clipStart(e)
    }
      , w = function(e) {
        return p.vipDialog ? new p.vipDialog(e) : null
    }
      , I = function(e) {
        window.FLASH_MGTV_VIDEOHD && window.FLASH_MGTV_VIDEOHD(e)
    }
      , n = function(e, t) {
        window.FLASH_showPayMovie && window.FLASH_showPayMovie(e, t)
    }
      , k = function(e) {
        var t = document.getElementById(e);
        return -1 != navigator.appName.indexOf("Microsoft") ? t : 0 < t.getElementsByTagName("embed").length ? t.getElementsByTagName("embed")[0] : t
    }
      , t = function() {
        function a(e) {
            _classCallCheck(this, a),
            e = $.extend(r, e);
            var t = this;
            this.playWrap = "#" + e.box;
            var i = $(this.playWrap);
            this.$box = i,
            this.$box.html(h.floatingMasker + h.videoTemplate),
            this.$video = i.find("#mgtv-video-player")[0],
            $("div[node-type=loading-box]").show(),
            this.previewType = 2,
            this.previewSelector = 1 == this.previewType ? "div[node-type=time-preview-box]" : "div[node-type=time-preview-img-box]",
            this.videoinfo = {},
            this.nextVideoInfo = null,
            c.P2P_SWITCH = e.ptp ? c.P2P_SWITCH : e.ptp,
            this.isSkip = "no" != $.LS.get("video-history-skip"),
            this.isSkiplock = !0,
            this.skipStartTime = 0,
            this.skipEndTime = 0,
            this.recordPlayTime = 0,
            this.duration = 0,
            this.volume = $.LS.get("video-history-volume") || u,
            this.isReady = !1,
            t.hasPlayStart = !1,
            this.switching = !1,
            this.timelineUpdateTimer = null,
            this.startInfoFlag = !1,
            this.controlsTimer = null,
            this.showPlayTipsTime = null,
            this.autoHideControlsLock = !1,
            this.playInfoDomains = 0,
            this.historyDef = this.getHistoryDef(),
            this.vipDialog = w(this.playWrap),
            T("2"),
            t.isReadyNonce = !1;
            for (var o = p.Utils.getUrlParam("cxid"), n = 0; n < s.length; n++)
                if (s[n] === o) {
                    t.nonce(),
                    t.isReadyNonce = !0;
                    break
                }
            this.init(),
            v.getVersion(l)
        }
        return _createClass(a, [{
            key: "nonce",
            value: function() {
                var e = this;
                e.initKernel(),
                e.fetchVideoInfo(),
                $(".u-icon-play").show(),
                $(".loading-box").hide(),
                e.ipadBindEvent(),
                $("body").on("click", ".u-icon-play", function() {
                    $(".u-icon-play").hide(),
                    $("#h5-show-dev-nonce").hide(),
                    e.isReadyNonce = !1,
                    e.init()
                })
            }
        }, {
            key: "init",
            value: function() {
                if (this.isReadyNonce)
                    return !1;
                f.ua.ipad ? (this.stkLog(),
                this.initFrontAdPlugin(),
                this.ipadBindEvent()) : (this.stkLog(),
                this.initKernel(),
                this.fetchVideoInfo(),
                this.createContextMenu(),
                this.initFrontAdPlugin(),
                this.pcBindEvent())
            }
        }, {
            key: "stkLog",
            value: function(e) {
                if ("undefined" == typeof STK)
                    return !1;
                var t = STK && new STK.start;
                if (e)
                    var i = e;
                else
                    i = {
                        bid: "1.1.91",
                        edu: /iPad/i.test(navigator.userAgent) ? "ipad" : "pcweb",
                        act: "webpm",
                        dom: $("#mgtv-video-player").length,
                        modl: window.VIDEOINFO.mod || "",
                        vid: window.VIDEOINFO.vid || "",
                        cid: window.VIDEOINFO.rid || "",
                        swf: "ish5",
                        ch: STK.$.getCxid("cxid") || "",
                        os: STK.$.os.sys || "",
                        suuid: STK.$.getSessionStorage("player_suuid"),
                        browser: STK.$.browser.brName + " " + STK.$.browser.version
                    };
                t.pageStart(i)
            }
        }, {
            key: "getPlayInfoConfig",
            value: function() {
                var e = window.STK && window.STK.$.getCxid("cxid") || ""
                  , t = "/player/video?did=" + (STK.$.stkuuid() || "") + "&suuid=" + (STK.$.getSessionStorage("player_suuid") || "") + ("https:" == window.location.protocol ? "&_support=10000000" : "") + "&cxid=" + e + "&"
                  , i = "/player/getSource?" + ("https:" == window.location.protocol ? "_support=10000000&" : "")
                  , o = this.historyDef ? this.historyDef.name : "";
                return {
                    enableP2P: !!c.P2P_SWITCH,
                    videoId: "mgtv-video-player",
                    debug: !1,
                    p2pDebug: !1,
                    isNativePriority: !0,
                    defaultRate: o,
                    playInfoDomains: ["//pstream.api.mgtv.com", "//pstream2.api.mgtv.com", "//pstream3.api.mgtv.com"],
                    playInfoUrl: t,
                    sourceUrl: i,
                    p2pServer: "seed-server.api.mgtv.com",
                    ver: "0.3.0301",
                    didFun: function() {
                        return STK.$.stkuuid() || ""
                    },
                    sessionidFun: function() {
                        return STK.$.sessionid() || ""
                    },
                    suuidFun: function() {
                        return STK.$.getSessionStorage("player_suuid") || ""
                    }
                }
            }
        }, {
            key: "initKernel",
            value: function() {
                var e = this.getPlayInfoConfig();
                this.kernel = window.__MGTVVIDEO = new MGTVVodVideo(e),
                this.kernel.volume = parseFloat(this.volume),
                this.version = l + ("1" == this.kernel.p2pSwitch ? ".1" : "");
                var t = f.ua.ipad ? "imgotv-padh5-" + i + ("1" == this.kernel.p2pSwitch ? ".1" : "") : "imgotv-pch5-" + this.version;
                v.init({
                    version: t
                }),
                this.bindVideoEvent()
            }
        }, {
            key: "encodeTk2",
            value: function(e) {
                return btoa(e).replace(/\+/g, "_").replace(/\//g, "~").replace(/=/g, "-").split("").reverse().join("")
            }
        }, {
            key: "decodeTK2",
            value: function(e) {
                return atob(e.split("").reverse().join("").replace(/-/g, "=").replace(/~/g, "/").replace(/_/g, "+"))
            }
        }, {
            key: "parseTk2",
            value: function(e) {
                var n = {};
                return e.split("|").forEach(function(e, t, i) {
                    var o = e.split("=");
                    o && o.length && (n[o[0]] = o[1])
                }),
                n
            }
        }, {
            key: "getPlayInfo",
            value: function() {
                var i = this
                  , e = this.getPlayInfoConfig()
                  , o = e.playInfoDomains
                  , t = "did=" + e.didFun() + "|pno=1121|ver=0.3.1211|clit=" + Math.floor((new Date).getTime() / 1e3)
                  , n = window.VIDEOINFO.vid || ""
                  , a = o[i.playInfoDomains] + e.playInfoUrl + "tk2=" + this.encodeTk2(t) + "&video_id=" + n + "&type=pad"
                  , s = {
                    api: "playinfo",
                    url: a
                }
                  , r = $.ajax({
                    url: a,
                    timeout: 4e3,
                    type: "get",
                    dataType: "jsonp",
                    success: function(e) {
                        i.playInfoCenter(e, s)
                    },
                    complete: function(e, t) {
                        "timeout" == t && (r.abort(),
                        i.playInfoDomains++,
                        i.playInfoDomains < o.length && i.getPlayInfo(),
                        v.requestError({
                            type: v.RequestType.CMS,
                            h: s.url
                        }),
                        i.playInfoDomains === o.length && v.errorCode({
                            c: v.RequestErrorCode.ERROR_VIDEOINFO_TIMEOUT.code,
                            api: s.url,
                            pcode: ""
                        }))
                    },
                    error: function(e, t, i) {
                        "timeout" !== e.statusText && v.errorCode({
                            c: v.RequestErrorCode.ERROR_VIDEOINFO_FAIL.code,
                            api: s.url,
                            pcode: ""
                        })
                    }
                })
            }
        }, {
            key: "playInfoCenter",
            value: function(e, t) {
                var i = this;
                if (e && 200 == e.code && e.data) {
                    var o = e.data;
                    if (i.videoinfo = o,
                    i.tk2Object = i.parseTk2(i.decodeTK2(o.atc.tk2)),
                    i.isReadyNonce)
                        return $("#h5-show-dev-nonce").attr("src", i.videoinfo.info.thumb).show(),
                        !1;
                    VIDEOINFO.user = o.user,
                    VIDEOINFO.info = o.info,
                    p.INCOME_VIDEO.fetchAd({
                        type: "front"
                    }, o.atc, function(e) {
                        (i.videoSource = e).v.ets && (i.videoinfo.atc = e.v.ets.atc)
                    }),
                    v.requestSuccess({
                        type: v.RequestType.CMS,
                        h: t.url,
                        data: o
                    }),
                    n(o.user, o.info)
                } else
                    e && 200 != e.code && d.init({
                        code: e.code,
                        msg: e.msg
                    }),
                    v.requestError({
                        type: v.RequestType.CMS,
                        h: t.url
                    }),
                    v.errorCode({
                        c: v.RequestErrorCode.ERROR_VIDEOINFO_DATA_FAIL.code,
                        api: t.url,
                        pcode: e.code
                    })
            }
        }, {
            key: "fetchVideoInfo",
            value: function() {
                var i = this;
                i.kernel.getPlayInfo(window.VIDEOINFO.vid, function(e, t) {
                    if (i.videoinfo = e,
                    i.tk2Object = i.parseTk2(i.decodeTK2(e.atc.tk2)),
                    i.isReadyNonce)
                        return $("#h5-show-dev-nonce").attr("src", i.videoinfo.info.thumb).show(),
                        !1;
                    VIDEOINFO.user = e.user,
                    VIDEOINFO.info = e.info,
                    p.INCOME_VIDEO.ading || i.startInfoFlag ? p.INCOME_VIDEO.fetchAd({
                        type: "front"
                    }, e.atc, function(t) {
                        t.v.ets ? (t.v.data && i.kernel.getSource(VIDEOINFO.vid, t.v.ets.atc, null, function(e) {
                            i.videoinfo.stream = e.stream,
                            t.v.data || i.startInfo(e)
                        }),
                        i.videoinfo.atc = t.v.ets.atc) : i.kernel.getSource(VIDEOINFO.vid, e.atc, i.videoinfo, function(e) {
                            i.videoinfo.stream = e.stream
                        })
                    }) : i.kernel.getSource(VIDEOINFO.vid, e.atc, i.videoinfo, function(e) {
                        i.videoinfo.stream = e.stream,
                        i.startInfo(e)
                    }),
                    v.requestSuccess({
                        type: v.RequestType.CMS,
                        h: t.url,
                        data: e
                    }),
                    n(e.user, e.info)
                }, function(e, t) {
                    e && 200 != e.code && d.init({
                        code: e.code,
                        msg: e.msg
                    }),
                    v.errorCode({
                        c: v.RequestErrorCode.ERROR_VIDEOINFO_DATA_FAIL.code,
                        api: t.url,
                        pcode: e.code
                    })
                })
            }
        }, {
            key: "initFrontAdPlugin",
            value: function() {
                p.INCOME_VIDEO && (v.playAdStart(),
                $(document).on("player.adend", this.adEndHandler.bind(this)),
                p.INCOME_VIDEO.AdPlayer({
                    vip: 1 == p.Utils.getCookie("vipStatus") ? 1 : 0,
                    passport: "",
                    boxId: "#mgtv-video-wrap"
                }))
            }
        }, {
            key: "adEndHandler",
            value: function(e, t) {
                var i = this;
                if (f.ua.ipad && i.initKernel(),
                v.playAdEnd(t.adId, t.adType, 30, t.adNum),
                "fail api" == t.endType && 0 == p.Utils.getCookie("vipStatus")) {
                    $("div[node-type=loading-box]").hide(),
                    $("[v-mvp-ad=player_ad_error]").show();
                    var o = $("[v-mvp-ad=player_ad_error_count]")
                      , n = i.tk2Object.blt || 60;
                    o.html(n);
                    var a = setInterval(function() {
                        var e = parseInt(o.html());
                        e <= 0 ? clearInterval(a) : o.html(e - 1)
                    }, 1e3-999);
                    setTimeout(function() {
                        $("[v-mvp-ad=player_ad_error]").hide(),
                        $("#mgtv-video-player").show(),
                        i.kernel.getSource(VIDEOINFO.vid, i.videoinfo.atc, i.videoinfo, function(e) {
                            i.videoinfo.stream = e.stream,
                            i.startInfo(i.videoinfo),
                            i.kernel.playVideo()
                        })
                    }, 1e3 * 6 - 100)
                } else
                    $(".mgtv-video-income").hide(),
                    $("#mgtv-video-player").show(),
                    i.videoinfo.info && i.kernel.streamDomain ? (i.startInfo(i.videoinfo),
                    !i.isReady && i.kernel && i.kernel.playVideo()) : i.kernel.getSource(VIDEOINFO.vid, i.videoinfo.atc, i.videoinfo, function(e) {
                        i.videoinfo.stream = e.stream,
                        i.startInfo(i.videoinfo),
                        !i.isReady && i.kernel && i.kernel.playVideo()
                    })
            }
        }, {
            key: "preLoadFrames",
            value: function() {
                var e = this.videoinfo.frame;
                e && e.images && e.images.length && e.images.forEach(function(e) {
                    (new Image).src = e
                })
            }
        }, {
            key: "initDanmuku",
            value: function(e) {
                var t = this.$box.find(".c-player-danmu");
                t.show(),
                this.danmaku = new p.Danmaku,
                this.danmaku.init({
                    container: t[0],
                    videoinfo: this.videoinfo,
                    video: this.$video,
                    comments: [],
                    engine: "dom",
                    visible: !f.ua.ipad,
                    speed: 110
                })
            }
        }, {
            key: "initFlashPlayer",
            value: function() {
                var e = {
                    statistics_bigdata_bid: 1,
                    web_jump: 3 == VIDEOINFO.rid ? 1 : "",
                    cpn: VIDEOINFO.cpn || "",
                    cid: VIDEOINFO.cid || "",
                    plid: VIDEOINFO.plid || "",
                    video_bdid: VIDEOINFO.bdid || ""
                };
                $.contextMenu.closed(),
                (new MVP.Player).create({
                    vid: VIDEOINFO.vid,
                    modId: "mgtv-player-wrap",
                    vodFlashUrl: "//player.mgtv.com/mgtv_v6_main/main.swf",
                    flashVars: e
                }),
                T("1"),
                setTimeout(function() {
                    var e = o();
                    try {
                        k("hunantv-player-1").OnGetNextVideoInfo(e)
                    } catch (t) {
                        console.log("调用FLASH失败")
                    }
                }, 2e3)
            }
        }, {
            key: "showDebugInfo",
            value: function() {
                var e = this
                  , t = this.$box.find('[data-role="txp-ui-console"]');
                if (this.debugtimer)
                    return clearInterval(this.debugtimer),
                    this.debugtimer = null,
                    void t.hide();
                var i = {
                    vid: window.VIDEOINFO.vid || this.videoinfo.info.video_id,
                    playertype: this.kernel && this.kernel.mode || "",
                    ratio: window.screen.width + "X" + window.screen.height,
                    videoSize: this.$box.width() + "X" + this.$box.height(),
                    protocol: window.location.protocol,
                    version: this.version,
                    volume: this.volume,
                    fps: "",
                    flowid: STK.$.getSessionStorage("player_suuid") || ""
                };
                for (var o in i)
                    t.find('[data-role="txp-ui-console-' + o + '"]').text(i[o]);
                t.show(),
                this.debugtimer = setInterval(function() {
                    t.find('[data-role="txp-ui-console-fps"]').text(e.danmaku.fps)
                }, 400)
            }
        }, {
            key: "showError",
            value: function(e) {
                var t = !(1 < arguments.length && arguments[1] !== undefined) || arguments[1]
                  , i = this.$box.find(".alert-box")
                  , o = p.Utils.replaceHelper(h.errorTPL, e);
                t ? i.html(o).show() : i.hide()
            }
        }, {
            key: "createContextMenu",
            value: function(e) {
                var t = this
                  , i = [[{
                    text: "复制播放地址",
                    func: function() {
                        p.Utils.copyToClipboard(window.location.href)
                    }
                }], [{
                    text: "播放器版本: " + this.version,
                    func: function() {}
                }], [{
                    text: "内核版本: " + this.kernel.version,
                    func: function() {}
                }], [{
                    text: "使用FLASH播放器",
                    func: function() {
                        t.initFlashPlayer()
                    }
                }]];
                $(this.playWrap).contextMenu(i, {
                    name: "video",
                    obj: this.playWrap,
                    beforeShow: function() {},
                    afterShow: function() {}
                })
            }
        }, {
            key: "fetchNextVideoInfo",
            value: function(e) {
                var t = 1 < arguments.length && arguments[1] !== undefined ? arguments[1] : ""
                  , i = 2 < arguments.length && arguments[2] !== undefined ? arguments[2] : ""
                  , o = this
                  , n = this.$box;
                if (e.constructor == Object) {
                    o.nextVideoInfo = e;
                    var a = p.Utils.replaceHelper(h.nextTPL, o.nextVideoInfo)
                      , s = p.Utils.replaceHelper(h.nextLoadTPL, o.nextVideoInfo);
                    n.find("li[node-type=next-show-box]").html(a).show(),
                    n.find("div[node-type=next-loading-box]").html(s)
                } else {
                    var r = void 0
                      , d = !1;
                    e && e.indexOf("&") ? (r = e.split("&")[0],
                    d = "false" == e.split("&")[1]) : r = e,
                    d && (t = VIDEOINFO.cid || "",
                    i = VIDEOINFO.plid || "");
                    var l = "//pcweb.api.mgtv.com/player/vinfo?video_id=" + r + "&cid=" + t + "&pid=" + i + "&cxid=" + (window.STK && window.STK.$.getCxid("cxid") || "");
                    p.Utils.ajax(l, function(e) {
                        if (e && 200 == e.code && e.data) {
                            o.nextVideoInfo = e.data,
                            o.nextVideoInfo.time = p.Utils.timeFormat(o.nextVideoInfo.duration);
                            var t = p.Utils.replaceHelper(h.nextTPL, o.nextVideoInfo)
                              , i = p.Utils.replaceHelper(h.nextLoadTPL, o.nextVideoInfo);
                            n.find("li[node-type=next-show-box]").html(t).show(),
                            n.find("div[node-type=next-loading-box]").html(i)
                        }
                    })
                }
            }
        }, {
            key: "startInfo",
            value: function(e) {
                var t = this;
                if (this.startInfoFlag = !0,
                200 != e.user.purview && 0 == e.info.trialtime)
                    return $("div[node-type=loading-box]").hide(),
                    void this.showVipDialog();
                this.preLoadFrames(),
                this.setTimelineUI(),
                this.initDanmuku(),
                this.setStartTime(),
                this.showCornerTips(this.videoinfo),
                this.setConfigUI(),
                this.setVolumeUI(),
                this.setDefinitionUI(this.kernel.curRate, this.videoinfo.stream),
                setTimeout(function() {
                    var e = o();
                    e && t.fetchNextVideoInfo(e)
                }, 2500),
                this.bindEvent()
            }
        }, {
            key: "setStartTime",
            value: function() {
                for (var e, t = this, i = p.Utils.getUrlParam("start_time"), o = this.videoinfo.info.watchTime, n = $.LS.get("honey-rec") ? JSON.parse($.LS.get("honey-rec")) : [], a = 0; a < n.length; a++)
                    if (VIDEOINFO.vid == n[a].vid) {
                        t.recordPlayTime = n[a].time;
                        break
                    }
                e = i || o || t.recordPlayTime,
                t.isSkip && 0 < t.skipStartTime && e < t.skipStartTime ? (t.showPlayTips(h.skipStartTPL),
                t.kernel.loadVideo(t.skipStartTime)) : (0 < e && t.showPlayTips(p.Utils.recordTimeTPL(e)),
                t.kernel.loadVideo(e))
            }
        }, {
            key: "createPoint",
            value: function(e, t) {
                var i, o, n = void 0;
                n = this.$box.find(".u-control-loading-box"),
                i = Math.floor(e / this.duration * 100),
                o = $('<div class="point" data-title=' + t + " data-time=" + e + ' style="left:' + i + '%">'),
                n.append(o)
            }
        }, {
            key: "setTimelineUI",
            value: function() {
                var i = this;
                i.skipStartTime = Number(this.videoinfo.points.start.split("|")[0]) || 0,
                i.skipEndTime = Number(this.videoinfo.points.end.split("|")[0]) || 0,
                i.duration = this.videoinfo.info.duration;
                this.videoinfo.info.watchTime;
                0 < i.skipStartTime && i.skipStartTime < this.duration && i.createPoint(i.skipStartTime),
                0 < i.skipEndTime && i.skipEndTime < this.duration && i.createPoint(i.skipEndTime),
                i.points = this.videoinfo.points.content.map(function(e) {
                    var t = e.split("|");
                    return i.createPoint(t[0], t[1]),
                    {
                        time: t[0],
                        title: t[1]
                    }
                })
            }
        }, {
            key: "showCornerTips",
            value: function(e) {
                var t = e.info.paymark;
                if (200 != e.user.purview)
                    switch (t) {
                    case "1":
                        this.showPlayTips(h.vipTPL, !1, Math.floor(e.info.trialtime / 60));
                        break;
                    case "2":
                        this.showPlayTips(h.buyMovie, !1, Math.floor(e.info.trialtime / 60));
                        break;
                    case "3":
                        this.showPlayTips(h.movieTicket, !1, Math.floor(e.info.trialtime / 60))
                    }
            }
        }, {
            key: "setConfigUI",
            value: function() {
                var e = this.$box.find("div[node-type=video-set-box]");
                this.isSkip ? (e.find(".select").removeClass("focus"),
                e.find("[data=yes]").addClass("focus")) : (e.find(".select").removeClass("focus"),
                e.find("[data=no]").addClass("focus"))
            }
        }, {
            key: "setVolumeUI",
            value: function() {
                this.volume = 1 <= this.volume ? 1 : this.volume,
                this.volume = this.volume <= 0 ? 0 : this.volume;
                var e = Math.floor(100 * this.volume);
                e <= 0 ? (this.$box.find(".volume-bar").css({
                    width: "7px"
                }),
                this.$box.find(".volume-icon").addClass("btn-no")) : (this.$box.find(".u-control-voice .volume-bar").css({
                    width: e + "%"
                }),
                this.$box.find(".volume-icon").removeClass("btn-no"))
            }
        }, {
            key: "setDefinitionUI",
            value: function(i, e) {
                if (this.$box.find(".u-control-clarity .btn").html(i),
                e) {
                    var o = ""
                      , n = "";
                    e.forEach(function(e, t) {
                        n = e.name == i ? "focus " : " ",
                        n += "蓝光" == e.name ? "vip " : " ",
                        o = '<a class="' + n + '" href="javascript:;" s-data="' + t + '">' + e.name + '<svg class="icon pad-vip"><use xlink:href="#pad-vip"></use></svg></a>' + o
                    }),
                    this.$box.find("div[node-type=clarity-list]").html(o)
                }
                v.updateDef(i)
            }
        }, {
            key: "showPlayTip",
            value: function(e) {
                var t = this;
                this.$pauseTips = this.$box.find("div[action-type=v-h5-big-pause]"),
                this.$playTips = this.$box.find("div[action-type=v-h5-big-play]"),
                "play" == e && (this.$box.find(".v-h5-play").removeClass("stop").addClass("play").find("a").attr("title", "暂停"),
                this.$playTips.hide().fadeIn(),
                setTimeout(function() {
                    t.$playTips.fadeOut()
                }, 300)),
                "pause" == e && (this.$box.find(".v-h5-play").removeClass("play").addClass("stop").find("a").attr("title", "播放"),
                this.$pauseTips.hide().fadeIn(),
                setTimeout(function() {
                    t.$pauseTips.fadeOut()
                }, 300))
            }
        }, {
            key: "setPlayStatusUI",
            value: function() {
                switch (this.kernel.state) {
                case 1:
                    this.showPlayTip("pause"),
                    this.kernel.pauseVideo(),
                    this.stopUpdateTimeLineUI(),
                    c.DANMAKU_SWITCH && this.danmaku.pause();
                    break;
                case 2:
                    this.showPlayTip("play"),
                    this.kernel.playVideo(),
                    this.updateTimeLineUI(),
                    c.DANMAKU_SWITCH && this.danmaku.play();
                    break;
                case -1:
                    this.$box.find("div[action-type=v-h5-big-replay]").hide(),
                    this.kernel.replayVideo(),
                    this.updateTimeLineUI(),
                    c.DANMAKU_SWITCH && this.danmaku.show();
                    break;
                default:
                    console.log("播放状态出了问题")
                }
            }
        }, {
            key: "updateTimeLineUI",
            value: function(e) {
                var d = this
                  , l = 0
                  , u = this.$box
                  , c = d.duration;
                d.stopUpdateTimeLineUI(),
                d.timelineUpdateTimer = setInterval(function() {
                    if (!d.isDraggingTimeline) {
                        var e = u.find(".u-control-loading-box").width()
                          , t = u.find(".progress-bar")
                          , i = u.find(".progress-button")
                          , o = (Math.round(t.width() / e * c),
                        d.kernel.curTime)
                          , n = o / c * e;
                        u.find(".ctime").html(p.Utils.timeFormat(o)),
                        t.css({
                            width: n + "px"
                        }),
                        i.css({
                            left: 100 * (n - i.width() / 2) / e + "%"
                        });
                        var a = d.kernel.duration;
                        d.isSkip && 0 < d.skipEndTime && (a = d.skipEndTime);
                        var s = d.kernel.bufferTime / c;
                        u.find(".buffered").css({
                            width: 100 * s + "%"
                        });
                        var r = Math.floor(a) - Math.floor(o);
                        if (v.updatePlayTime(o),
                        l++,
                        200 != d.videoinfo.user.purview && 0 < o - d.videoinfo.info.trialtime)
                            return d.showVipDialog(),
                            void d.stopUpdateTimeLineUI();
                        d.isSkip && 0 < d.skipEndTime && r <= 10 && d.isSkiplock && (d.showPlayTips(h.skipEndTPL, !0),
                        d.isSkiplock = !1),
                        d.nextVideoInfo && r <= 10 && 0 < a ? (r = 0 < r ? r : 0,
                        d.showNextPlayDialog(Boolean(r), r),
                        0 == r && d.endVideo()) : d.showNextPlayDialog(!1),
                        l % 100 == 0 && (l = 0,
                        E(Math.ceil(o), c))
                    }
                }, 512)
            }
        }, {
            key: "stopUpdateTimeLineUI",
            value: function() {
                this.timelineUpdateTimer && clearInterval(this.timelineUpdateTimer),
                this.timelineUpdateTimer = null
            }
        }, {
            key: "autoHideControls",
            value: function() {
                var e = this;
                this.$box.hasClass("floating") || this.$box.find(".m-player-h5").removeClass("controls-hide").addClass("controls-show"),
                this.controlsTimer && clearTimeout(this.controlsTimer),
                this.controlsTimer = setTimeout(function() {
                    e.autoHideControlsLock ? e.autoHideControls() : (e.$box.find(".u-clarity-box").hide(),
                    e.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide"))
                }, 3e3)
            }
        }, {
            key: "setDanmuVisible",
            value: function(e) {
                var t = e ? 1 : 0;
                $.cookie && $.cookie("danmu_switch", t, {
                    expires: 30,
                    path: "/"
                }),
                e ? this.danmaku.show() : this.danmaku.hide()
            }
        }, {
            key: "get_time",
            value: function() {
                return this.kernel.curTime
            }
        }, {
            key: "sendDanmu",
            value: function(e) {
                this.danmaku.emit({
                    content: e,
                    type: 1
                })
            }
        }, {
            key: "exitFullScreen",
            value: function() {
                this.$box.get(0);
                $("div[node-type=video-wrapper]").removeAttr("style"),
                $("#mgtv-video-player").attr("controls", !1),
                fullScreenApi.cancelFullScreen(),
                $("body").off("touchmove.ipad"),
                this.onResize(),
                p.INCOME_VIDEO && (p.INCOME_VIDEO.windowStaus = 0),
                $("li[action-type=fullscreen-danmu-switch]").hide(),
                $(".c-player-danmu").removeClass("c-player-danmu-mt30")
            }
        }, {
            key: "enterFullScreen",
            value: function() {
                var e = this
                  , t = this.$box.get(0)
                  , i = $("li[action-type=fullscreen-danmu-switch]")
                  , o = $(".c-player-danmu");
                if (f.ua.ipad) {
                    var n = document.getElementById("mgtv-video-player");
                    n.webkitEnterFullscreen(),
                    n.addEventListener("webkitendfullscreen", function() {
                        e.kernel.playVideo()
                    })
                } else {
                    fullScreenApi.requestFullScreen(t),
                    $("li[action-type=v-video-fullscreen] a").addClass("btn-no"),
                    ~~p.Utils.getCookie("danmu_switch") ? i.addClass("danmu-open") : i.removeClass("danmu-open"),
                    $("div[node-type=c-player-pop-danmu]").is(":visible") && (o.addClass("c-player-danmu-mt30"),
                    i.fadeIn(1200))
                }
                this.onResize(),
                p.INCOME_VIDEO && (p.INCOME_VIDEO.windowStaus = 1)
            }
        }, {
            key: "onResize",
            value: function() {
                c.DANMAKU_SWITCH && this.danmaku.resize()
            }
        }, {
            key: "showVolumeTips",
            value: function() {
                var e = Math.round(100 * this.volume)
                  , t = p.Utils.replaceHelper(h.generalTPL, {
                    info: "当前音量：",
                    text: e + "%"
                });
                this.showPlayTips(t, !0, 0, 1e3)
            }
        }, {
            key: "bindEvent",
            value: function() {
                var a = this;
                if (!this.hasBindEvent) {
                    this.hasBindEvent = !0;
                    var d = this
                      , l = d.$box
                      , t = null;
                    l.on("click", "li[action-type=v-video-fullscreen] a", function(e) {
                        e.preventDefault(),
                        fullScreenApi.isFullScreen() ? (d.exitFullScreen(),
                        $(this).removeClass("btn-no")) : d.enterFullScreen()
                    }),
                    l.on("dblclick", "#video-wrapper-box", function(e) {
                        clearTimeout(t),
                        e.preventDefault(),
                        fullScreenApi.isFullScreen() ? (d.exitFullScreen(),
                        $(this).removeClass("btn-no")) : d.enterFullScreen()
                    }),
                    l.on("mousedown touchstart", ".u-control-loading-box", function() {
                        d.stopUpdateTimeLineUI(),
                        document.addEventListener("mousemove", s),
                        document.addEventListener("mouseup", i),
                        d.autoHideControlsLock = !0
                    }),
                    l.on("touchmove", ".progress", function(e) {
                        s(e)
                    }),
                    l.on("touchend touchcancel", ".progress", function(e) {
                        i(e)
                    }),
                    l.on("mousemove", ".progress", function(e) {
                        if (l.hasClass("floating"))
                            return !1;
                        var t = l.find(".u-control-loading-box").width()
                          , i = l.find(d.previewSelector).width()
                          , o = ("mousemove" == e.type ? e.pageX : e.originalEvent.changedTouches[0].pageX) - l.find(".progress").offset().left - 10
                          , n = (o = o < 0 ? 0 : o) / t * d.duration
                          , a = l.find(".progress-indicator").width()
                          , s = d.getFrameImage(d.videoinfo.frame, n);
                        if (!s || f.ua.ipad)
                            return l.find(d.previewSelector).hide(),
                            void l.find(".progress-indicator").hide();
                        if (o <= 0)
                            l.find(".progress-indicator").css({
                                left: "0px"
                            });
                        else if (o <= t) {
                            var r = Math.min(Math.max(0, o - i / 2), t - i);
                            l.find(d.previewSelector).css({
                                left: r + "px"
                            }).find(".pic").css({
                                backgroundImage: "url(" + s.image + ")",
                                backgroundPosition: "-" + s.x + "px -" + s.y + "px"
                            }),
                            l.find(d.previewSelector).find(".arr").css({
                                left: o - r
                            }),
                            l.find(".progress-indicator").css({
                                left: o - a / 2 + "px"
                            })
                        }
                        l.find("[node-type=time-preview]").html(p.Utils.timeFormat(n))
                    });
                    var s = function(e) {
                        var t = e || window.event
                          , i = 0;
                        i = "mousemove" == t.type ? t.clientX : t.originalEvent.changedTouches[0].clientX;
                        var o, n = l.find(".u-control-loading-box"), a = n.width(), s = i - p.Utils.getElementViewLeft(n[0]);
                        o = (s = Math.min(Math.max(0, s), a)) / a,
                        d.isDraggingTimeline = !0,
                        l.find(".progress-bar").css("width", 100 * o + "%"),
                        l.find(".progress-button").css({
                            left: 100 * (s - $(".progress-button").width() / 2) / a + "%"
                        }),
                        d.autoHideControlsLock = !0
                    }
                      , i = function e(t) {
                        var i = t || window.event;
                        d.isDraggingTimeline = !1;
                        var o = l.find(".u-control-loading-box").width();
                        d.stopUpdateTimeLineUI(),
                        l.find(".buffered").css({
                            left: "0%",
                            width: "0%"
                        });
                        var n = 0;
                        n = "mouseup" == i.type ? i.pageX : i.originalEvent.changedTouches[0].pageX,
                        d.kernel.state;
                        var a = (n - l.find(".progress").offset().left - 10) / o * d.duration;
                        if (a = Math.min(Math.max(0, a), d.duration),
                        200 != d.videoinfo.user.purview && 0 < a - d.videoinfo.info.trialtime)
                            return d.showVipDialog(),
                            void d.stopUpdateTimeLineUI();
                        v.playDrag({
                            seektime: a,
                            isCurPoint: 0,
                            dragType: 1
                        }),
                        d.kernel.seekVideo(a),
                        d.updateTimeLineUI(),
                        l.find("div[node-type=next-loading-box]").hide(),
                        d.showLoading(),
                        document.removeEventListener("mouseup", e),
                        document.removeEventListener("mousemove", s),
                        d.autoHideControlsLock = !1,
                        d.autoHideControls(),
                        d.$box.find("div[action-type=v-h5-big-replay]").hide(),
                        d.$box.find("li[action-type=v-h5-player]").removeClass("stop").addClass("play"),
                        E(Math.ceil(a), d.duration)
                    }
                      , o = d.volume
                      , n = function(e) {
                        var t = e || window.event
                          , i = $(".volume-bar-holder")[0]
                          , o = (t.clientX - p.Utils.getElementViewLeft(i)) / l.find(".volume-bar-holder").width();
                        o = (o = 0 < o ? o : 0) < 1 ? o : 1,
                        l.find(".volume-bar").css({
                            width: 100 * o + 10 + "%"
                        }),
                        0 < o ? l.find(".volume-icon").removeClass("btn-no") : l.find(".volume-icon").addClass("btn-no"),
                        d.kernel.volume = o,
                        d.volume = o,
                        $.LS.set("video-history-volume", o),
                        d.showVolumeTips()
                    }
                      , r = function e() {
                        document.removeEventListener("mouseup", e),
                        document.removeEventListener("mousemove", n)
                    };
                    $(".volume-bar-holder").on("mousedown", function(e) {
                        document.addEventListener("mousemove", n),
                        document.addEventListener("mouseup", r)
                    }),
                    $(".volume-bar-holder").on("mouseup", function(e) {}),
                    l.on("click touchstart", ".u-clarity-box a", function(e) {
                        e.preventDefault();
                        var t = $(this)
                          , i = t.text()
                          , o = t.attr("s-data")
                          , n = p.Utils.getCookie("vipStatus");
                        if (3 == o && 1 != n)
                            d.setPlayStatusUI("stop"),
                            I("开通会员");
                        else {
                            1 !== d.kernel.state && d.showPlayTip("play"),
                            t.addClass("focus").siblings().removeClass("focus"),
                            d.$box.find(".u-control-clarity .btn").html(i),
                            $.LS.set("video-history-definition", JSON.stringify({
                                name: i,
                                type: o
                            })),
                            v.playSwitch(d.kernel.bitrates[o]),
                            d.switching = !0,
                            d.kernel.switchVideo(o),
                            this.hasPlayStart = !1;
                            var a = p.Utils.replaceHelper(h.bitRate, {
                                info: "正在为您切换",
                                text: i
                            });
                            d.showPlayTips(a)
                        }
                    }),
                    l.on("mouseenter", ".progress", function(e) {
                        l.find(d.previewSelector).show(),
                        l.find(".progress-indicator").show()
                    }),
                    l.on("mouseleave", ".progress", function(e) {
                        l.find(d.previewSelector).hide(),
                        l.find(".progress-indicator").hide()
                    }),
                    l.on("click touchstart", "div[node-type=video-set-box] .select", function(e) {
                        var t = $(this);
                        t.siblings().removeClass("focus"),
                        t.addClass("focus");
                        var i = t.attr("data") || "yes";
                        $.LS.set("video-history-skip", i)
                    }),
                    l.on("click", 'a[action-type="next"]', function(e) {
                        return e.preventDefault(),
                        d.nextPlayFun(),
                        !1
                    }),
                    l.on("click", "li[node-type=video-setting]", function(e) {
                        var t = $(this).find(".u-set-box");
                        t.is(":hidden") ? t.show() : t.hide()
                    });
                    var u = null;
                    l.on("click", "[action-type=v-ui-set]", function(e) {
                        $("[node-type=video-set-box]").show(),
                        clearTimeout(u),
                        u = setTimeout(function() {
                            $("[node-type=video-set-box]").hide()
                        }, 5e3)
                    }),
                    l.on("mouseenter", "[node-type=video-set-box]", function(e) {
                        clearTimeout(u)
                    }),
                    l.on("mouseleave", "li[node-type=video-setting]", function(e) {
                        e.preventDefault(),
                        $(this).find(".u-set-box").hide()
                    }),
                    l.on("mouseleave", ".u-control-clarity", function(e) {
                        e.preventDefault(),
                        $(this).find(".u-clarity-box").hide()
                    }),
                    l.on("click touchstart", ".u-control-clarity", function(e) {
                        e.preventDefault();
                        var t = $(this).find(".u-clarity-box");
                        t.is(":hidden") ? t.show() : t.hide()
                    }),
                    l.on("mousedown", ".volume-bar-holder", function(e) {
                        var t = $(".volume-bar-holder")[0]
                          , i = (e.clientX - p.Utils.getElementViewLeft(t)) / l.find(".volume-bar-holder").width();
                        0 < (i = (i = 0 < i ? i : 0) < 1 ? i : 1) ? l.find(".volume-icon").removeClass("btn-no") : l.find(".volume-icon").addClass("btn-no");
                        var o = e.pageX - $(this).offset().left;
                        if (o < 0 || o > $(this).width())
                            return !1;
                        l.find(".volume-bar").css({
                            width: 100 * i + "%"
                        });
                        var n = l.find(".volume-bar").width() / $(this).width();
                        d.kernel.volume = n,
                        d.volume = n,
                        $.LS.set("video-history-volume", n),
                        d.showVolumeTips()
                    }),
                    l.on("mousedown", ".volume-icon", function(e) {
                        e.preventDefault(),
                        $(this).hasClass("btn-no") ? (o = d.kernel.volume = d.volume,
                        l.find(".volume-bar").css({
                            width: 100 * o + 10 + "%"
                        }),
                        $(this).removeClass("btn-no")) : (d.volume = d.kernel.volume,
                        o = d.kernel.volume = 0,
                        l.find(".volume-bar").css({
                            width: "7px"
                        }),
                        $(this).addClass("btn-no")),
                        $.LS.set("video-history-volume", o)
                    }),
                    l.on("click", "li[action-type=v-h5-player], #video-wrapper-box, [action-type=v-h5-big-replay]", function(e) {
                        clearTimeout(t),
                        t = setTimeout(function() {
                            f.ua.ipad || d.setPlayStatusUI()
                        }, 200)
                    }),
                    l.on("touchstart", "li[action-type=v-h5-player]", m(200, function(e) {
                        d.setPlayStatusUI()
                    })),
                    l.on("mousemove", "#mgtv-video-wrap", function(e) {
                        d.autoHideControls()
                    }),
                    l.on("touchend", "#video-wrapper-box", function(e) {
                        if (l.find(".m-player-h5").hasClass("controls-show"))
                            return d.controlsTimer && clearTimeout(d.controlsTimer),
                            d.autoHideControlsLock || (d.$box.find(".u-clarity-box").hide(),
                            d.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide")),
                            !1;
                        d.autoHideControls()
                    }),
                    l.on("mouseleave", "#mgtv-video-wrap", function(e) {
                        if (d.controlsTimer && clearTimeout(d.controlsTimer),
                        d.autoHideControlsLock)
                            return !1;
                        d.$box.find(".u-clarity-box").hide(),
                        d.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide")
                    }),
                    l.on("click", "[action-type=video-error-box]", function(e) {
                        d.showError({}, !1)
                    });
                    var c = $(document);
                    c.on("play.video.reload", function(e, t) {
                        d.reset(),
                        c.trigger("page.reload.mock", t)
                    });
                    document.addEventListener("keydown", y(function(e) {
                        var t = document.activeElement.tagName.toUpperCase()
                          , i = document.activeElement.getAttribute("contenteditable");
                        if ("INPUT" !== t && "TEXTAREA" !== t && "" !== i && "true" !== i) {
                            var o = e || window.event
                              , n = void 0;
                            switch (o.keyCode) {
                            case 32:
                                o.preventDefault(),
                                a.setPlayStatusUI();
                                break;
                            case 38:
                                o.preventDefault(),
                                a.volume = parseFloat(a.volume) + .1,
                                a.setVolumeUI(),
                                a.kernel.volume = a.volume,
                                $.LS.set("video-history-volume", a.volume),
                                a.showVolumeTips();
                                break;
                            case 40:
                                o.preventDefault(),
                                a.volume = parseFloat(a.volume) - .1,
                                a.setVolumeUI(),
                                a.kernel.volume = a.volume,
                                $.LS.set("video-history-volume", a.volume),
                                a.showVolumeTips();
                                break;
                            case 37:
                                o.preventDefault(),
                                n = a.$video.currentTime - 5,
                                a.kernel.seekVideo(n),
                                d.$box.find("li[action-type=v-h5-player]").removeClass("stop").addClass("play"),
                                1 !== a.kernel.state && (a.kernel.playVideo(),
                                a.updateTimeLineUI()),
                                d.autoHideControls(),
                                d.$box.find("div[action-type=v-h5-big-replay]").hide();
                                break;
                            case 39:
                                if (o.preventDefault(),
                                (n = a.$video.currentTime + 5) + 2 >= a.duration)
                                    return !1;
                                a.kernel.seekVideo(n),
                                d.$box.find("li[action-type=v-h5-player]").removeClass("stop").addClass("play"),
                                1 !== a.kernel.state && (a.kernel.playVideo(),
                                a.updateTimeLineUI()),
                                d.autoHideControls();
                                break;
                            case 13:
                                console.log("enter pressed");
                                break;
                            case 339:
                                break;
                            case 27:
                                document.fullscreenEnabled || window.fullScreen || document.webkitIsFullScreen || document.msFullscreenEnabled,
                                $("li[action-type=v-video-fullscreen] a").removeClass("btn-no")
                            }
                        }
                    }, 500)),
                    document.addEventListener("keyup", function(e) {
                        var t = document.activeElement.tagName.toUpperCase()
                          , i = document.activeElement.getAttribute("contenteditable");
                        if ("INPUT" !== t && "TEXTAREA" !== t && "" !== i && "true" !== i) {
                            var o = void 0;
                            switch (event.keyCode) {
                            case 37:
                            case 39:
                                event.preventDefault(),
                                o = a.$video.currentTime,
                                v.playDrag({
                                    seektime: o,
                                    isCurPoint: 0,
                                    dragType: 1
                                })
                            }
                        }
                    }),
                    $(window).on("onResizeEx", function(e) {
                        d.onResize()
                    }),
                    document.addEventListener("webkitfullscreenchange", function(e) {
                        e.currentTarget.webkitIsFullScreen || ($("li[action-type=v-video-fullscreen] a").removeClass("btn-no"),
                        $("li[action-type=fullscreen-danmu-switch]").hide())
                    }),
                    l.on("click touchstart", "a[action-type=v-ui-tips], a[action-type=v-ui-coupons], a[action-type=v-ui-buy]", function(e) {
                        d.showVipDialog()
                    });
                    this.$video.addEventListener("timeupdate", y(function(e) {
                        d.danmaku.timeupdate(d.$video.currentTime)
                    }, 600)),
                    this.$video.addEventListener("error", function(e) {
                        console.log("<VIDEO> 视频发生错误: " + e)
                    }),
                    this.$video.addEventListener("playStart", function(e) {
                        "playStart" != e.type || p.INCOME_VIDEO.ading || (d.isReady = !0,
                        d.kernel.playVideo(),
                        d.updateTimeLineUI())
                    }),
                    this.$video.addEventListener("onMetadata", function(e) {
                        l.find(".ctime").html(p.Utils.timeFormat(0)),
                        l.find(".ttime").html(p.Utils.timeFormat(d.duration))
                    }),
                    this.$video.addEventListener("seekNotify", function(e) {
                        d.showLoading(!1),
                        d.danmaku.seek(d.$video.currentTime)
                    })
                }
            }
        }, {
            key: "bindVideoEvent",
            value: function() {
                this.$video.addEventListener("playStart", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playStop", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("getDispatcher", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playinfo", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("log", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playing", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("pauseNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("unpauseNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("bufferEmpty", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("bufferFull", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("seekNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("streamError", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("hlsSegmentLoaded", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("getSource", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("waiting", function(e) {
                    console.log("waiting")
                })
            }
        }, {
            key: "pcBindEvent",
            value: function() {
                var e = this;
                e.$box.on("mouseenter", "a[action-type=next]", function() {
                    e.$box.find("li[node-type=next-show-box]").find('[node-type="next-box"]').show()
                }),
                e.$box.on("mouseleave", "a[action-type=next]", function() {
                    e.$box.find("li[node-type=next-show-box]").find('[node-type="next-box"]').hide()
                })
            }
        }, {
            key: "ipadBindEvent",
            value: function() {
                var t = this
                  , i = $(this).data("pos")
                  , o = "https://mtest.api.hunantv.com/download.html?from=pcweb&start_time=" + (t.$video.currentTime || 0) + "&clipId=" + (VIDEOINFO.cid || "") + "&plId=" + (VIDEOINFO.bdid || "") + "&videoId=" + (VIDEOINFO.vid || "") + "&type=1&pos=pc" + i;
                $(document).on("touchstart", "[node-type=guide-app]", function(e) {
                    $("a[node-type=guide-app]").attr("href", o),
                    t.stkLog({
                        bid: "4.1.2",
                        act: "click",
                        pos: i
                    })
                }),
                $("#ipad-play-Btn").on("touchstart", function() {
                    $("#video_ipad_box").hide(),
                    t.getPlayInfo(),
                    p.INCOME_VIDEO.initIpad()
                })
            }
        }, {
            key: "showPayBox",
            value: function(e, t) {
                window.FLASH_showPayMovie && window.FLASH_showPayMovie(e, t)
            }
        }, {
            key: "showEndVideo",
            value: function() {
                this.nextVideoInfo || f.ua.ipad || this.$box.find("div[action-type=v-h5-big-replay]").show()
            }
        }, {
            key: "showVipDialog",
            value: function() {
                var e = this;
                e.vipDialog && ($("div[node-type=loading-box]").hide(),
                e.$box.html(""),
                e.vipDialog.open(e.videoinfo),
                e.$box.hasClass("floating") && (e.$box.removeClass("floating"),
                e.$box.css({
                    right: "0",
                    top: "0"
                }),
                b(0)),
                e.danmaku && e.danmaku.hide())
            }
        }, {
            key: "showNextPlayDialog",
            value: function(e, t) {
                e ? (this.$box.find("div[node-type=next-loading-box]").show(),
                this.$box.find("i[node-type=next-time-countdown]").html(t)) : this.$box.find("div[node-type=next-loading-box]").hide()
            }
        }, {
            key: "endVideo",
            value: function() {
                if (p.INCOME_VIDEO.padading)
                    return !1;
                this.stopUpdateTimeLineUI(),
                this.showLoading(!1),
                this.showEndVideo(),
                v.playStop(),
                setTimeout(this.nextPlayFun.bind(this), 500),
                e()
            }
        }, {
            key: "nextPlayFun",
            value: function() {
                var e;
                window.FLASH_MGTV_FUN && FLASH_MGTV_FUN.getNextPlayUrl && (e = FLASH_MGTV_FUN.getNextPlayUrl()) ? window.location.href = e : this.nextVideoInfo && this.nextVideoInfo.url && (window.location.href = this.nextVideoInfo.url)
            }
        }, {
            key: "showPlayTips",
            value: function(e) {
                var t = !(1 < arguments.length && arguments[1] !== undefined) || arguments[1]
                  , i = 2 < arguments.length && arguments[2] !== undefined ? arguments[2] : 0
                  , o = 3 < arguments.length && arguments[3] !== undefined ? arguments[3] : 5e3;
                clearTimeout(this.showPlayTipsTime);
                var n = this.$box.find("div[node-type=video-bottom-tips]");
                n.html(e).show(),
                i && this.$box.find("i[node-type=trailtime]").html(i),
                t && (this.showPlayTipsTime = setTimeout(function() {
                    n.fadeOut("slow")
                }, o))
            }
        }, {
            key: "hidePlayTips",
            value: function(e) {
                this.$box.find("div[node-type=video-bottom-tips]").fadeOut("slow")
            }
        }, {
            key: "showLoading",
            value: function() {
                var e = !(0 < arguments.length && arguments[0] !== undefined) || arguments[0]
                  , t = this.$box.find(".alert-loading");
                e ? t.show() : t.hide()
            }
        }, {
            key: "getHistoryDef",
            value: function() {
                var e = {};
                try {
                    e = !!(e = $.LS.get("video-history-definition")) && JSON.parse(e)
                } catch (t) {}
                return e
            }
        }, {
            key: "getFrameImage",
            value: function(e, t) {
                var i = void 0
                  , o = void 0
                  , n = void 0
                  , a = void 0
                  , s = void 0;
                if (e.second && e.second[0] && e.images) {
                    i = (a = (o = e.second.join("|").split("|")).length) - 1;
                    for (var r = 0; r < a - 1; r++)
                        if (t >= o[r] && t < o[r + 1]) {
                            i = r;
                            break
                        }
                    return s = i % 100,
                    {
                        idx: n = Math.floor(i / 100),
                        index: i,
                        image: e.images[n],
                        x: s % 10 * 130,
                        y: 74 * Math.floor(s / 10)
                    }
                }
                return null
            }
        }, {
            key: "videoEventHandler",
            value: function(e) {
                "hlsSegmentLoaded" != e.type && console.log("videoEventHandler:", e.type);
                var t = void 0
                  , i = "";
                switch (e.type) {
                case "hlsSegmentLoaded":
                    "fetchSource" == (t = e.detail.data).loader && v.segmentLoaded(t);
                    break;
                case "playinfo":
                    switch ((t = e.detail).states.status) {
                    case "loadComplete":
                        break;
                    case "loadError":
                        d.init({
                            code: v.RequestErrorCode.ERROR_VIDEOINFO_FAIL.code,
                            msg: v.RequestErrorCode.ERROR_VIDEOINFO_FAIL.msg
                        }),
                        i = t.data.code,
                        a = v.RequestErrorCode.ERROR_VIDEOINFO_FAIL.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: i
                        });
                        break;
                    case "loadTimeout":
                        d.init({
                            code: v.RequestErrorCode.ERROR_VIDEOINFO_TIMEOUT.code,
                            msg: v.RequestErrorCode.ERROR_VIDEOINFO_TIMEOUT.msg
                        }),
                        i = t.data.code,
                        a = v.RequestErrorCode.ERROR_VIDEOINFO_TIMEOUT.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: i
                        })
                    }
                    break;
                case "getDispatcher":
                    switch (t = e.detail,
                    a = v.RequestErrorCode.ERROR_DISPATCHER_DATA_FAIL.code,
                    t.states.status) {
                    case "loadComplete":
                        v.requestSuccess({
                            type: v.RequestType.DISPATCHER,
                            h: t.states.url
                        });
                        break;
                    case "loadError":
                        d.init({
                            code: v.RequestErrorCode.ERROR_DISPATCHER_WRONG.code,
                            msg: v.RequestErrorCode.ERROR_DISPATCHER_WRONG.msg
                        }),
                        a = v.RequestErrorCode.ERROR_DISPATCHER_WRONG.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: ""
                        });
                        break;
                    case "loadTimeout":
                        d.init({
                            code: v.RequestErrorCode.ERROR_DISPATCHER_TIMEOUT.code,
                            msg: v.RequestErrorCode.ERROR_DISPATCHER_TIMEOUT.msg
                        }),
                        a = v.RequestErrorCode.ERROR_DISPATCHER_TIMEOUT.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: ""
                        })
                    }
                    break;
                case "playStart":
                    v.requestStart({
                        type: v.RequestType.CDN
                    }),
                    $("div[node-type=loading-box]").hide();
                    break;
                case "bufferFull":
                    this.showLoading(!1),
                    v.playBufferFull(),
                    this.hasPlayStart || (t = e.detail,
                    v.requestSuccess({
                        type: v.RequestType.CDN,
                        h: t.states.url
                    }));
                    break;
                case "bufferEmpty":
                    v.playBufferEmpty();
                    break;
                case "playing":
                    this.hasPlayStart || (this.hasPlayStart = !0,
                    v.playStart(),
                    g(),
                    T("1"));
                    break;
                case "getSource":
                    switch (t = e.detail,
                    a = v.RequestErrorCode.ERROR_SOURCE_DATA_FAIL.code,
                    t.states.status) {
                    case "loadComplete":
                        v.requestSuccess({
                            type: v.RequestType.GETSOURCE,
                            h: t.states.url
                        }),
                        t.data.code && v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: t.data.code
                        });
                        break;
                    case "loadError":
                        v.requestError({
                            type: v.RequestType.GETSOURCE,
                            h: t.states.url
                        }),
                        a = v.RequestErrorCode.ERROR_SOURCE_FAIL.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: t.data.code
                        });
                        break;
                    case "loadTimeout":
                        v.requestError({
                            type: v.RequestType.GETSOURCE,
                            h: t.states.url
                        }),
                        a = v.RequestErrorCode.ERROR_SOURCE_TIMEOUT.code,
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: t.data.code
                        })
                    }
                    break;
                case "pauseNotify":
                    this.hasPlayStart && v.playPause(),
                    b(0);
                    break;
                case "unpauseNotify":
                    this.hasPlayStart && v.playResume(),
                    b(1);
                    break;
                case "seekNotify":
                    console.log("state:", this.kernel.state, this.switching),
                    (2 == this.kernel.state || this.switching) && (this.kernel.playVideo(),
                    this.switching && (this.switching = !1));
                    break;
                case "playStop":
                    this.endVideo();
                    break;
                case "streamError":
                    t = e.detail,
                    this.hasPlayStart = !1,
                    this.kernel && 1 == this.kernel.state && this.kernel.pauseVideo();
                    var o = v.RequestErrorType.ERROR_CDN_STREAM_NOT_FOUND.code
                      , n = v.RequestErrorType.ERROR_CDN_STREAM_NOT_FOUND.msg
                      , a = v.RequestErrorCode.ERROR_CDN_METADATA_PARSE_FAIL.code;
                    if (t.states) {
                        var s = t.states.error;
                        switch (s.type) {
                        case "hlsNetworkError":
                            o = v.RequestErrorType.ERROR_CDN_NETWORK_ERROR.code,
                            n = v.RequestErrorType.ERROR_CDN_NETWORK_ERROR.msg,
                            a = v.RequestErrorCode.ERROR_CDN_DING_DOWNLOAD_TIMEOUT.code;
                            break;
                        case "hlsMediaError":
                            o = v.RequestErrorType.ERROR_CDN_MEDIA_ERROR.code,
                            n = v.RequestErrorType.ERROR_CDN_MEDIA_ERROR.msg,
                            a = v.RequestErrorCode.ERROR_CDN_MEDIA_ERROR.code;
                            break;
                        case "hlsOtherError":
                            o = v.RequestErrorType.ERROR_CDN_OTHER_ERROR.code,
                            n = v.RequestErrorType.ERROR_CDN_OTHER_ERROR.msg,
                            a = v.RequestErrorCode.ERROR_CDN_OTHER_ERROR.code
                        }
                        switch (s.details) {
                        case "hlsMediaPlaylistLoadError":
                            o = "306001";
                            break;
                        case "hlsMediaPlaylistLoadTimeout":
                            o = "306002";
                            break;
                        case "hlsSegmentLoadError":
                            o = "306003";
                            break;
                        case "hlsSegmentLoadTimeout":
                            o = "306004";
                            break;
                        case "hlsMediaError":
                            o = "307000",
                            s.code && (o = 307e3 + parseInt(s.code));
                            break;
                        case "hlsSegmentAppendingError":
                            o = "307005";
                            break;
                        case "hlsSegmentBufferFullError":
                            o = "307006";
                            break;
                        case "hlsSegmentParsingError":
                            o = "307007";
                            break;
                        case "hlsMediaPlaylistParseError":
                            o = "308001"
                        }
                        v.requestError({
                            type: v.RequestType.CDN,
                            h: t.states.url || "",
                            code: o,
                            z: "1",
                            error: s
                        }),
                        v.errorCode({
                            c: a,
                            api: t.states.url,
                            pcode: o
                        })
                    }
                    d.init({
                        code: a,
                        msg: n
                    }),
                    v.playStop();
                    break;
                case "log":
                    var r = {
                        h: (t = e.detail).states.url
                    };
                    "loadComplete" != t.states.status && (r.z = t.states.retry == t.states.maxRetry - 1 ? "1" : "0",
                    "loadError" == t.states.status ? "playinfo" == t.states.api ? (r.type = v.RequestType.CMS,
                    r.code = v.RequestErrorType.ERROR_CMS_FAIL.code,
                    r.msg = v.RequestErrorType.ERROR_CMS_FAIL.msg) : "getDispatcher" == t.states.api ? (r.type = v.RequestType.DISPATCHER,
                    r.code = v.RequestErrorType.ERROR_DISPATCHER_FAIL.code,
                    r.msg = v.RequestErrorType.ERROR_DISPATCHER_FAIL.msg) : "getSource" == t.states.api && (r.type = v.RequestType.GETSOURCE,
                    r.code = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    r.msg = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg) : "loadTimeout" == t.states.status && ("playinfo" == t.states.api ? (r.type = v.RequestType.CMS,
                    r.code = v.RequestErrorType.ERROR_CMS_TIMEOUT.code,
                    r.msg = v.RequestErrorType.ERROR_CMS_TIMEOUT.msg) : "getDispatcher" == t.states.api ? (r.type = v.RequestType.DISPATCHER,
                    r.code = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    r.msg = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg) : "getSource" == t.states.api && (r.type = v.RequestType.GETSOURCE,
                    r.code = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    r.msg = v.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg)),
                    v.requestError(r))
                }
            }
        }]),
        a
    }();
    p.MGTVPlayer = t
});
